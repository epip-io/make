FROM golang:1.12.9-alpine

RUN apk update && \
    apk --update add \
      bash \
      ca-certificates \
      coreutils \
      curl \
      git \
      gettext \
      grep \
      jq \
      libc6-compat \
      make \
      openssh-client \
      py-pip && \
    git config --global advice.detachedHead false

ADD ./ /opt/make

ENV INSTALL_PATH /usr/local/bin

WORKDIR /opt/make

ENTRYPOINT [ "/opt/make/bin/plugin.sh" ]