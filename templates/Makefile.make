export SHELL = /bin/bash
export MAKE_ORG ?= epip-io
export MAKE_PROJECT ?= make
export MAKE_BRANCH ?= master
export MAKE_PATH ?= $(shell until [ -d "$(MAKE_PROJECT)" ] || [ "`pwd`" == '/' ]; do cd ..; done; pwd)/$(MAKE_PROJECT)
-include $(MAKE_PATH)/Makefile

.PHONY : init
## Init make
init::
	@curl --retry 5 --fail --silent --retry-delay 1 https://bitbucket.org/$(MAKE_ORG)/$(MAKE_PROJECT)/raw/$(MAKE_BRANCH)/bin/install.sh | \
		bash -s "$(MAKE_ORG)" "$(MAKE_PROJECT)" "$(MAKE_BRANCH)"
    export MAKE_PATH=$(cd $(MAKE_PROJECT) && pwd)

.PHONY : clean
## Clean make
clean::
	@[ "$(MAKE_PATH)" == '/' ] || \
	 [ "$(MAKE_PATH)" == '.' ] || \
	   echo rm -rf $(MAKE_PATH)
