# make

This `make` is a collection of Makefiles to facilitate building Golang projects, Dockerfiles, Helm charts, and more.
It's designed to work with CI/CD systems such as Travis CI, CircleCI and Jenkins.
