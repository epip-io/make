ifeq ($(wildcard .git),)
  ifeq ($(DEBUG),true)
    $(warning disabling semver bootstrapping)
  endif
else
  export SEMVER_MAJOR ?= $(shell echo "$(GIT_RELEASE_VERSION)" | awk -F. '{ print substr($$1,2); }')
  export SEMVER_MINOR ?= $(shell echo "$(GIT_RELEASE_VERSION)" | awk -F. '{ print $$2; }')
  export SEMVER_PATCH ?= $(shell echo "$(GIT_RELEASE_VERSION)" | awk -F. '{ print $$3; }')

  export SEMVER_RELEASE ?= $(GIT_RELEASE_VERSION)

  export SEMVER_NEXT ?= v$(SEMVER_MAJOR).$(SEMVER_MINOR).$(shell expr $(SEMVER_PATCH) + 1)

  export SEMVER_CURRENT ?= $(GIT_BRANCH_TAG)

endif