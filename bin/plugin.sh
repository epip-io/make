#!/bin/bash

set -eo pipefail

export SCRIPT_PATH=$(cd "$(dirname "$0")" 2>&1 && pwd | sed -e 's~/bin~~')
export PATH=${PATH}:${SCRIPT_PATH}/bin

TARGETS=${PLUGIN_TARGETS:-help}

if [[ ! -z "${PLUGIN_GIT_SSH_KEY}" ]]; then
    mkdir -p ${HOME}/.ssh
    echo "${PLUGIN_GIT_SSH_KEY}" > ${HOME}/.ssh/id_rsa
    chmod 600 ${HOME}/.ssh/id_rsa
    unset PLUGIN_GIT_SSH_KEY

    ssh-keyscan -t rsa ${PLUGIN_GIT_SERVER:-bitbucket.org} | tee ${HOME}/.ssh/known_hosts
fi

if [ ! -f "Makefile" ]; then
    echo "Copying Top Level Make"
    cp ${SCRIPT_PATH}/templates/Makefile.make Makefile
fi

if [ "${DEBUG}" = "true" ]; then
    echo "\${PATH}=${PATH}"
    echo "\${SCRIPT_PATH}=${SCRIPT_PATH}"
    echo "\${TARGETS}=${TARGETS}"
    echo "\$@=$@"
fi

if [[ -z "$@" ]]; then
    for TARGET in $(echo $TARGETS | tr "," "\n");
    do
        make ${TARGET};
    done
else
    if make help/targets | grep "^$1$"; then
        make $@
    else
        exec "$@"
    fi
fi
