#!/bin/bash
export MAKE_ORG=${1:-epip-io}
export MAKE_PROJECT=${2:-make}
export MAKE_BRANCH=${3:-master}
export BITBUCKET_REPO="https://bitbucket.org/${MAKE_ORG}/${MAKE_PROJECT}.git"

if [ "$MAKE_PROJECT" ] && [ -d "$MAKE_PROJECT" ]; then
  echo "Removing existing $MAKE_PROJECT"
  rm -rf "$MAKE_PROJECT"
fi

echo "Cloning ${BITBUCKET_REPO}#${MAKE_BRANCH}..."
git clone -b $MAKE_BRANCH $BITBUCKET_REPO
