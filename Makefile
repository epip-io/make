export MAKE_FILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
export MAKE_PARENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MAKE_FILE_PATH))))
export MAKE_PATH ?= $(abspath $(dir $(MAKE_FILE_PATH)))
export OS ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
export SELF ?= $(MAKE)
export PATH := $(MAKE_PATH)/vendor:$(PATH)
export DOCKER_BUILD_FLAGS ?=
export DEBUG ?=

ifeq ($(CURDIR),$(realpath $(BUILD_HARNESS_PATH)))
export README_DEPS ?= docs/targets.md
export DEFAULT_HELP_TARGET = help/all
endif

include $(MAKE_PATH)/Makefile.*
include $(MAKE_PATH)/modules/*/bootstrap.Makefile*
include $(MAKE_PATH)/modules/*/Makefile*

ifndef TRANSLATE_COLON_NOTATION
%:
	@$(SELF) -s $(subst :,/,$@) TRANSLATE_COLON_NOTATION=false
endif